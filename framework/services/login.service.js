import { buildRequest } from '../../lib/index';

import { decorateService } from '../../lib/index';

const LoginVikunja = function LoginVikunja() {
  this.post = async function loginVikunja(params) {
    const req  = buildRequest;
    const r = await req().post('/api/v1/login').send(params);
    return r;
  };
};
decorateService(LoginVikunja);

export { LoginVikunja };
